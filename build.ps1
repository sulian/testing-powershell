<#
.Synopsis
Lance le script PowerShell depuis un fichier .gitlab-ci.yml en utilisant GitLab CI

.DESCRIPTION
Permet de lancer les tests unitaires lors d'un commit sur le serveur GitLab. Le fichier de 
configuration `.gitlab-ci.yml` ex�cute ce fichier pour lancer les tests avec le module
Pester via une ligne de commande.

Pas besoin d'exporter les r�sultats, on ignore donc pour le moment :
 - -OutputFile Reports\Test-Pester.xml
 - -OutputFormat NUnitXML 
Ni `-PassThru` ?

.PARAMETER Tasks : T�che(s) � ex�cuter

#>
param(
    [string[]]$Tasks
)

function runTests {
    # Verification si le module `Pester` est charg�
    $module = Get-Module -Name Pester
    If ($module.Name -notmatch "Pester") {
        Import-Module Pester
    }
    # Lance les tests en mode silencieux
    $files = Get-ChildItem .\Tests\ -File -recurse -Include *.ps1
    $results = Invoke-Pester -Path Tests -CodeCoverage $files -PassThru -Show None

    # Analyse des tests
    if ($results.FailedCount -gt 0) {
        Write-Output "  > $($results.FailedCount) tests failed. The build cannot continue."
        foreach ($result in $($results.TestResult | Where-Object {$_.Passed -eq $false} | Select-Object -Property Describe,Context,Name,Passed,Time)){
            Write-Output "    > $result"
        }
        Exit 1
    }

    # Couverture de code
    if ($results.CodeCoverage.NumberOfCommandsAnalyzed -ne 0){
        $coverage = [math]::Round($(100 - (($results.CodeCoverage.NumberOfCommandsMissed / $results.CodeCoverage.NumberOfCommandsAnalyzed) * 100)), 2);
        Write-Output "  > Code Coverage: $coverage%"
    }

}

foreach($task in $Tasks) {
    switch ($task) {
        'analyse' {
            Write-Host "Bienvenue"
            break;
        }
        'test' {
            Write-Host "Lancement des tests Pester"
            runTests
            break;
        }
        'release' {
            Write-Host "Mise en production"
            break;
        }
        Default {}
    }
}