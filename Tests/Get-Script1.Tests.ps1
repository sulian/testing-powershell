﻿$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. ".\$sut"

Describe "Get-Script1" {
    It "outputs 'Hello world!'" {
        Get-Script1 | Should Be 'Hello world!'
    }
}

Describe 'Basic Pester Tests' {
    It 'A test that should be true' {
        $true | Should -Be $true
    }
    <#
    It 'A test that should fail' {
        $false | Should -Be $true
    }
    #>
    It 'Another test that should be true' {
        $true | Should -Be $true
    }
}
