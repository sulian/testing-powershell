# Changelog
Tous les changements notables sur ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et suis les recommandations du [Versionnage Sémantique](http://semver.org/lang/fr/spec/v2.0.0.html).

## [Non publié]

---

## [0.1.0]

### Ajouté

- Ajout du CHANGELOG
- Contenu du README
- Fichiers liés au projet : *gitlab.-ci.yml*, *.gitignore*
- Script **build.ps1** : permet de lancer les tests unitaires depuis le serveur GitLab
- Tests unitaires :
  - Get-Script1 : exemple 1
