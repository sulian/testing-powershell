# Tests PowerShell

Mise en place d'une intégration continue avec GitLab pour les projets en PowerShell.

L'intégration continue passe par une validation du code source via des tests unitaires effectués avec [Pester](https://github.com/Pester/Pester).

## Suivi des modifications

Les modifications apportées au projet sont référencés dans le [journal des modifications](CHANGELOG.md).

## License

[Domaine publique](http://creativecommons.org/licenses/publicdomain/)

Copyright (c) 2020-present, Sulian Lanteri